package com.cmsland.rentcar.controller;

import com.cmsland.rentcar.TestData;
import com.cmsland.rentcar.Util;
import com.cmsland.rentcar.dao.Customer;
import com.cmsland.rentcar.service.CustomerService;
import com.cmsland.rentcar.service.CustomerServiceTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import java.net.URISyntaxException;

import static org.junit.Assert.*;
import static com.cmsland.rentcar.controller.RestfulConst.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CustomerControllerTest extends ControllerTest {

    @Autowired
    CustomerService service;

    @Test
    public void registerTest() throws URISyntaxException {
        Customer user = TestData.getExistUser();
        HttpEntity<Customer> request = new HttpEntity<>(user, null);
        try {
            restTemplate.put(super.getFullUri(REGISTER_PATH), request);
            fail();
        }
        catch(HttpClientErrorException ex) {
            assertEquals(HttpStatus.CONFLICT, ex.getStatusCode());
        }
        user.setName(TestData.INVALID_USERNAME);
        request = new HttpEntity<>(user, null);
        try {
            restTemplate.put(super.getFullUri(REGISTER_PATH), request);
        }
        catch(HttpClientErrorException ex) {
            fail();
        }
        assertNotNull(service.login(user));
        assertEquals(0, service.unRegister(user.getName(), user.getPassword()));
    }

    @Test
    public void loginTest() throws URISyntaxException {
        Customer user = TestData.getExistUser();
        HttpEntity<Customer> request = new HttpEntity<>(user, null);
        try {
            ResponseEntity<String> result = restTemplate.postForEntity(super.getFullUri(LOGIN_PATH), request, String.class);
            assertNotNull(result.getBody());
            System.out.println(result.getBody());
        } catch (HttpClientErrorException ex) {
            fail();
        }
        user.setName(TestData.INVALID_USERNAME);
        request = new HttpEntity<>(user, null);
        try {
            ResponseEntity<String> result = restTemplate.postForEntity(super.getFullUri(LOGIN_PATH), request, String.class);
            fail();
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.NOT_FOUND, ex.getStatusCode());
        }
    }

    @Test
    public void logoutTest() throws URISyntaxException {
        Customer user = TestData.getExistUser();
        HttpEntity<Customer> request = new HttpEntity<>(user, null);
        HttpHeaders headers = new HttpHeaders();
        try {
            ResponseEntity<String> result = restTemplate.postForEntity(super.getFullUri(LOGIN_PATH), request, String.class);
            assertNotNull(result.getBody());
            headers.add(Util.SESSION_HEAD, result.getBody());
        } catch (HttpClientErrorException ex) {
            fail();
        }
        request = new HttpEntity<>(user, headers);
        try {
            ResponseEntity<String> result = restTemplate.postForEntity(super.getFullUri(LOGOUT_PATH), request, String.class);
            assertNotNull(result.getBody());
        } catch (HttpClientErrorException ex) {
            fail();
        }
    }
}