package com.cmsland.rentcar.controller;

import org.junit.Test;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URISyntaxException;

import static com.cmsland.rentcar.controller.RestfulConst.DBNAME_PATH;
import static com.cmsland.rentcar.controller.RestfulConst.VERSION_PATH;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class VersionControllerTest extends ControllerTest {

    @Test
    public void testVersion() throws URISyntaxException {
        ResponseEntity<String> result = restTemplate.getForEntity(super.getFullUri(VERSION_PATH), String.class);
        //Verify request succeed
        Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
        Assert.assertEquals(true, result.getBody().contains("hello"));
    }
    @Test
    public void testGetDbName() throws Exception {
        ResponseEntity<String> result = restTemplate.getForEntity(super.getFullUri(DBNAME_PATH), String.class);
        System.out.println(result.getBody());
    }
}
