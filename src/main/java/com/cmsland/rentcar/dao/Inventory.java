package com.cmsland.rentcar.dao;

public class Inventory {

    private int modelId;
    private String model;
    private String engineNo;

    public Inventory(int _modelId, String _engNo, String _model) {
        this.modelId = _modelId;
        this.engineNo = _engNo;
        this.model = _model;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    @Override
    public String toString() {
        return String.format("Model ID: %d, Model: %s, Engine NO: %s", modelId, model, engineNo);
    }
}
