package com.cmsland.rentcar.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class StaticConfig {

    @Value("${config.db.name}")
    private String dbName;

    @Value("${config.version}")
    private String version;

    public String getVersion() {
        return version;
    }

    public String getDBName() {
        return dbName;
    }
}
