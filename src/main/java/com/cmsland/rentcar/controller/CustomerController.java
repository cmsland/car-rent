package com.cmsland.rentcar.controller;

import com.cmsland.rentcar.Util;
import com.cmsland.rentcar.dao.Customer;
import com.cmsland.rentcar.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.beans.factory.annotation.Autowired;

import static com.cmsland.rentcar.controller.RestfulConst.*;

@RestController
public class CustomerController {

    @Autowired
    CustomerService service;

    @PutMapping(path = REGISTER_PATH, consumes = JSON_FORMAT)
    public ResponseEntity<String> register(@RequestBody Customer user) {
        int result = service.register(user.getName(), user.getPassword());
        if (result > 0) return new ResponseEntity<>("user exsisted!", HttpStatus.CONFLICT);
        if (result < 0) return new ResponseEntity<>("internal db error!", HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>("ok", HttpStatus.CREATED);
    }

    @PostMapping(path = LOGIN_PATH, consumes = JSON_FORMAT)
    public ResponseEntity<String> login(@RequestBody Customer user) {
        String result = service.login(user);
        if (null == result) return new ResponseEntity<>("internal db error!", HttpStatus.INTERNAL_SERVER_ERROR);
        if (result.length() == 0) return new ResponseEntity<>("user not exsist!", HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping(path = LOGOUT_PATH, consumes = JSON_FORMAT)
    public ResponseEntity<String> logout(@RequestBody Customer user,
                                         @RequestHeader(name = Util.SESSION_HEAD, required = true) String session) {
        String result = service.logout(user.getName(), session);
        if (null == result) return new ResponseEntity<>("internal db error!", HttpStatus.INTERNAL_SERVER_ERROR);
        if (result.length() == 0) return new ResponseEntity<>("session not found!", HttpStatus.NOT_FOUND);
        return new ResponseEntity<>("ok", HttpStatus.OK);
    }
}
