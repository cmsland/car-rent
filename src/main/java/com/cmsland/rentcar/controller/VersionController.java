package com.cmsland.rentcar.controller;

import com.cmsland.rentcar.config.StaticConfig;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import static com.cmsland.rentcar.controller.RestfulConst.*;

@RestController
public class VersionController {

    @Autowired
    StaticConfig config;

    @GetMapping(VERSION_PATH)
    public ResponseEntity<String> version() {
        return new ResponseEntity<>("hello: " + config.getVersion() , HttpStatus.OK);
    }

    @GetMapping(DBNAME_PATH)
    public ResponseEntity<String> getDbName() {
        return new ResponseEntity<>(config.getDBName(), HttpStatus.OK);
    }

}