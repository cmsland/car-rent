package com.cmsland.rentcar.service;

import com.cmsland.rentcar.Util;
import com.cmsland.rentcar.dao.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.UUID;

@Service
public class CustomerService {

    Logger logger = LoggerFactory.getLogger(CustomerService.class);

    Connection conn = null;

    public CustomerService() {
        this.conn = Util.getDbConnection();
    }

    public CustomerService(Connection _conn) {
        this.conn = _conn;
    }

    public int register(String name, String password) {
        String sql = "select * from customer where name = ?;";
        try (PreparedStatement pstmt = this.conn.prepareStatement(sql)) {
            pstmt.setString(1, name);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) return 1;
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return -1;
        }
        sql = "insert into customer (name, password) values (?, ?);";
        try (PreparedStatement pstmt = this.conn.prepareStatement(sql)) {
            pstmt.setString(1, name);
            pstmt.setString(2, Util.string2Sha1(password));
            pstmt.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return -2;
        }
        return 0;
    }

    public int unRegister(String name, String password) {
        String sql = "delete from customer where name = ? and password = ?;";
        try (PreparedStatement pstmt = this.conn.prepareStatement(sql)) {
            pstmt.setString(1, name);
            pstmt.setString(2, Util.string2Sha1(password));
            pstmt.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return -2;
        }
        return 0;
    }

    public String logout(String userName, String session_id) {
        assert session_id != null;
        return this.login(userName, null, session_id);
    }

    public String login(Customer user) {
        return this.login(user.getName(), user.getPassword(), null);
    }

    private String login(String name, String password, String session_id) {
        int customerId = -1;
        String sql = "select customer_id from customer where name = ? and ";
        sql = sql + (session_id == null ? "password" : "session_id") + " = ?;";
        try (PreparedStatement pstmt = this.conn.prepareStatement(sql)) {
            pstmt.setString(1, name);
            pstmt.setString(2, session_id == null ? Util.string2Sha1(password) : session_id);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                customerId = rs.getInt("customer_id");
            }
            else {
                logger.warn(String.format("user: {%s} {%s} login faild!", name, Util.string2Sha1(password)));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return null;
        }
        if (customerId < 0) return "";

        if (session_id == null) /*login*/
            session_id = UUID.randomUUID().toString();
        else    /*logout*/
            session_id = null;
        sql = "update customer set session_id = ? where customer_id = ?;";
        try (PreparedStatement pstmt = this.conn.prepareStatement(sql)) {
            pstmt.setString(1, session_id);
            pstmt.setInt(2, customerId);
            pstmt.executeUpdate();
            logger.info(String.format("user: {%s} got session: {%s}", name, session_id));
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return null;
        }
        return session_id == null ? "empty" : session_id;
    }

}
