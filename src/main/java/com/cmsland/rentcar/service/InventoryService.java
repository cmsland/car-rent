package com.cmsland.rentcar.service;

import com.cmsland.rentcar.Util;
import com.cmsland.rentcar.dao.Inventory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static com.cmsland.rentcar.service.RentResult.*;

@Service
public class InventoryService {

    Logger logger = LoggerFactory.getLogger(CustomerService.class);

    Connection conn = null;


    public InventoryService() {
        this.conn = Util.getDbConnection();
    }

    public ArrayList<Inventory> getAvailable() {
        ArrayList<Inventory> list = new ArrayList<>();
        String sql = "select a.model_id, a.engine_no, b.name from inventory a inner join ";
        sql += "car_model b on a.model_id = b.model_id where a.rent_by is null order by a.model_id;";
        try (PreparedStatement pstmt = this.conn.prepareStatement(sql)) {
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(new Inventory(rs.getInt(1), rs.getString(2), rs.getString(3)));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return null;
        }
        return list;
    }

    public ArrayList<Inventory> getRentByCustomer(String session) throws SQLException {
        int customerId = -1;
        String sql = "select customer_id from customer where session_id IS NOT NULL and session_id = ?;";
        if (StringUtils.hasLength(session)) {
            try (PreparedStatement pstmt = this.conn.prepareStatement(sql)) {
                pstmt.setString(1, session);
                ResultSet rs = pstmt.executeQuery();
                if (rs.next()) {
                    customerId = rs.getInt(1);
                }
            } catch (SQLException e) {
                logger.error(e.getMessage());
                throw e;
            }
        }
        if (customerId < 0) return null;

        ArrayList<Inventory> list = new ArrayList<>();
        sql = "select a.model_id, a.engine_no, b.name from inventory a inner join car_model b " +
              "on a.model_id = b.model_id where a.rent_by = ? order by a.model_id;";
        try (PreparedStatement pstmt = this.conn.prepareStatement(sql)) {
            pstmt.setInt(1, customerId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(new Inventory(rs.getInt(1), rs.getString(2), rs.getString(3)));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw e;
        }
        return list;
    }

    public RentResult rentCar(Inventory car, String session) {
        int customerId = -1;
        String sql = "select customer_id from customer where session_id IS NOT NULL and session_id = ?;";
        if (StringUtils.hasLength(session)) {
            try (PreparedStatement pstmt = this.conn.prepareStatement(sql)) {
                pstmt.setString(1, session);
                ResultSet rs = pstmt.executeQuery();
                if (rs.next()) {
                    customerId = rs.getInt(1);
                }
            } catch (SQLException e) {
                logger.error(e.getMessage());
                return RentResult.DB_ERROR;
            }
        }
        if (customerId < 0) return INVALID_SESSION;

        sql = "select inventory_id, CASE WHEN rent_by IS NULL THEN -1 ELSE rent_by END from inventory "
            + "where model_id = ? and engine_no = ?;";
        try (PreparedStatement pstmt = this.conn.prepareStatement(sql)) {
            pstmt.setInt(1, car.getModelId());
            pstmt.setString(2, car.getEngineNo());
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                if (rs.getInt(2) > 0) return /*already rent*/ RENTOUT_ALREADY;
                return this.updateRent(rs.getInt(1), customerId);
            }
            return /*invalid inventory id*/ INVALID_INVENTORY;
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return DB_ERROR;
        }
    }

    public ReturnResult returnCar(Inventory car, String session) {
        int customerId = -1;
        String sql = "select customer_id from customer where session_id IS NOT NULL and session_id = ?;";
        try (PreparedStatement pstmt = this.conn.prepareStatement(sql)) {
            pstmt.setString(1, session);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                customerId = rs.getInt(1);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return ReturnResult.DB_ERROR;
        }
        if (customerId < 0) return ReturnResult.INVALID_SESSION;

        sql = "select inventory_id, CASE WHEN rent_by IS NULL THEN -1 ELSE rent_by END from inventory "
                + "where model_id = ? and engine_no = ?;";
        try (PreparedStatement pstmt = this.conn.prepareStatement(sql)) {
            pstmt.setInt(1, car.getModelId());
            pstmt.setString(2, car.getEngineNo());
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                if (rs.getInt(2) == customerId) {
                    return this.updateReturn(rs.getInt(1), customerId);
                }
                return ReturnResult.RENTBY_MISMATCH;
            }
            return /*invalid inventory id*/ ReturnResult.INVALID_INVENTORY;
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return ReturnResult.DB_ERROR;
        }
    }

    private RentResult updateRent(int inventoryId, int customerId) {
        String sql = "insert into tenancy (rent_by, inventory_id, rent_time) select ?, ?, datetime('now');";
        try (PreparedStatement pstmt = this.conn.prepareStatement(sql)) {
            pstmt.setInt(1, customerId);
            pstmt.setInt(2, inventoryId);
            pstmt.executeUpdate();

        } catch (SQLException e) {
            logger.error(e.getMessage());
            return DB_ERROR;
        }
        sql = "update inventory set rent_by = ? where rent_by IS NULL and inventory_id = ?;";
        try (PreparedStatement pstmt = this.conn.prepareStatement(sql)) {
            pstmt.setInt(1, customerId);
            pstmt.setInt(2, inventoryId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return DB_ERROR;
        }
        return SUCCESS;
    }

    private ReturnResult updateReturn(int inventoryId, int customerId) {
        int tenancyId = -1;
        String sql = "select max(tenancy_id) from tenancy where rent_by = ? and inventory_id = ?;";
        try (PreparedStatement pstmt = this.conn.prepareStatement(sql)) {
            pstmt.setInt(1, customerId);
            pstmt.setInt(2, inventoryId);
            ResultSet rs = pstmt.executeQuery();
            tenancyId = rs.getInt(1);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return ReturnResult.DB_ERROR;
        }
        if (tenancyId > 0) {
            sql = "update tenancy set return_time = datetime('now') where tenancy_id = ?;";
            try (PreparedStatement pstmt = this.conn.prepareStatement(sql)) {
                pstmt.setInt(1, tenancyId);
                pstmt.executeUpdate();
            } catch (SQLException e) {
                logger.error(e.getMessage());
                return ReturnResult.DB_ERROR;
            }
        }
        sql = "update inventory set rent_by = NULL where rent_by = ? and inventory_id = ?;";
        try (PreparedStatement pstmt = this.conn.prepareStatement(sql)) {
            pstmt.setInt(1, customerId);
            pstmt.setInt(2, inventoryId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return ReturnResult.DB_ERROR;
        }
        return ReturnResult.SUCCESS;
    }
}
