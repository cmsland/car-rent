package com.cmsland.rentcar.service;

public enum RentResult {
    SUCCESS,
    INVALID_SESSION,
    DB_ERROR,
    RENTOUT_ALREADY,
    INVALID_INVENTORY

}