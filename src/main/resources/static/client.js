    var session = null, username = null;
    var carList = null;
    var myList = null;

    function listAll() {
        $.ajax({
          url: "/rentcar/inventory",
          cache: false
        }).done(function(result) {
            carList = result;
            document.getElementById("list").innerHTML = '';
            for(var i=0; i<result.length; i++) {
                car = result[i];
                $("#list").append("<li><a href='javascript:rent("+i+")'>" + car["model"] + "[" + car["engineNo"] + "]</a></li>");
            }
        });
    }

    function listMyRent() {
        $.ajax({
          url: "/rentcar/inventory/rent",
          headers: {"X-SESSION-ID": session},
          cache: false
        }).done(function(result) {
            myList = result;
            document.getElementById("myList").innerHTML = '';
            for(var i=0; i<result.length; i++) {
                car = result[i];
                $("#myList").append("<li><a href='javascript:rnt("+i+")'>" + car["model"] + "[" + car["engineNo"] + "]</a></li>");
            }
        });
    }

    function rent(i) {
        if(session == null) {
            alert('Please sign in first!');
            return;
        }
        if(!confirm('Do you want to rent this model?')) return;
        var request = $.ajax({
          url: "/rentcar/inventory/rent",
          method: "POST",
          contentType: "application/json",
          data: JSON.stringify({modelId: carList[i].modelId, engineNo: carList[i].engineNo}),
          headers: {"X-SESSION-ID": session}
        });
        request.done(function(msg) {
            listAll();
            listMyRent();
            alert(msg);
        });
        request.fail(function( jqXHR, textStatus ) {
            alert( "Request failed: " + jqXHR.status );
        });
    }

    function rnt(i) {
        if(session == null) {
            alert('please sign in first!');
            return;
        }
        if(!confirm('Do you want to return this car?')) return;
        var request = $.ajax({
          url: "/rentcar/inventory/return",
          method: "POST",
          contentType: "application/json",
          data: JSON.stringify({modelId: myList[i].modelId, engineNo: myList[i].engineNo}),
          headers: {"X-SESSION-ID": session}
        });
        request.done(function(msg) {
            listAll();
            listMyRent();
            alert(msg);
        });
        request.fail(function( jqXHR, textStatus ) {
            alert( "Request failed: " + jqXHR.status );
        });
    }

    function reg() {
        var request = $.ajax({
          url: "/rentcar/register",
          method: "PUT",
          contentType: "application/json",
          data: JSON.stringify({name: $("#name").val(), password: $("#pwd").val()}),
        });
        request.done(function(msg) {
            alert(msg);
        });
        request.fail(function( jqXHR, textStatus ) {
          if (jqXHR.status == 409) {
            alert( "Username has been used, please use another one!" );
          }
          else {
            alert(jqXHR.status);
          }
        });
    }

    function login() {
        if(session == null) {
            var request = $.ajax({
              url: "/rentcar/login",
              method: "POST",
              contentType: "application/json",
              data: JSON.stringify({name: $("#name").val(), password: $("#pwd").val()}),
            });
            request.done(function(msg) {
                session = msg;
                username = $("#name").val();
                document.getElementById("btn").value = 'Sign Out';
                document.getElementById("userDiv").hidden = true;
                document.getElementById("btn2").hidden = true;
                listMyRent();
            });
            request.fail(function( jqXHR, textStatus ) {
              if (jqXHR.status == 404) {
                alert( "Login failed! \n\n If you are new user, please click 'Sign Up'" );
              }
            });
        } else {
           var request = $.ajax({
             url: "/rentcar/logout",
             method: "POST",
             contentType: "application/json",
             data: JSON.stringify({name: username, password: null}),
             headers: {"X-SESSION-ID": session}
           });
           request.done(function(msg) {
               session = null;
               username = null;
               document.getElementById("btn").value = 'Sign In';
               document.getElementById("userDiv").hidden = false;
               document.getElementById("myList").innerHTML = '';
           });
           request.fail(function( jqXHR, textStatus ) {
             if (jqXHR.status != 200) alert( "Request failed: " + jqXHR.status );
           });
       }
    }

    listAll();
