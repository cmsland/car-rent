FROM openjdk:8-jdk-alpine

ARG JAR_FILE
ADD target/${JAR_FILE} /u/app.jar

WORKDIR /u

EXPOSE 8899

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","app.jar"]
